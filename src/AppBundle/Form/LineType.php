<?php

namespace AppBundle\Form;

use AppBundle\Entity\Button;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LineType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lineName', null, array(
                'label' => 'Nazwa linii',
            ))
            ->add('buttonsToAdd', EntityType::class, array(
                'class' => Button::class,
                'choice_label' => 'buttonName',
                'multiple' => true,
                'expanded' => true,
                'mapped' => false,
                'label' => 'Przyciski wezwań'
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Line'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_line';
    }


}
