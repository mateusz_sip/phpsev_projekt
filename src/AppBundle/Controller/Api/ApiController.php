<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends FOSRestController
{
    /**
     * @Route("/api")
     */
    public function indexAction()
    {
        $em= $this->getDoctrine()->getManager();

        $lines = $em->getRepository('AppBundle:Line')->findAll();

        return JsonResponse::create($lines, 200);
    }
}