<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Button;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Button controller.
 *
 * @Route("admin/buttons")
 */
class ButtonController extends Controller
{
    /**
     * Lists all button entities.
     *
     * @Route("/", name="button_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $buttons = $em->getRepository('AppBundle:Button')->findAll();

        return $this->render('button/index.html.twig', array(
            'buttons' => $buttons,
        ));
    }

    /**
     * Creates a new button entity.
     *
     * @Route("/new", name="button_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $button = new Button();
        $form = $this->createForm('AppBundle\Form\ButtonType', $button);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($button);
            $em->flush();

            return $this->redirectToRoute('button_index', array('id' => $button->getId()));
        }

        return $this->render('button/new.html.twig', array(
            'button' => $button,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing button entity.
     *
     * @Route("/{id}/edit", name="button_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Button $button)
    {
        $deleteForm = $this->createDeleteForm($button);
        $editForm = $this->createForm('AppBundle\Form\ButtonType', $button);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('button_index');
        }

        return $this->render('button/edit.html.twig', array(
            'button' => $button,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a button entity.
     *
     * @Route("/{id}", name="button_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Button $button)
    {
        $form = $this->createDeleteForm($button);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $em->remove($button);
            $em->flush();


        return $this->redirectToRoute('button_index');
    }

    /**
     * Creates a form to delete a button entity.
     *
     * @param Button $button The button entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Button $button)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('button_delete', array('id' => $button->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
