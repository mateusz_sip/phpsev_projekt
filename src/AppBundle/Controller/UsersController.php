<?php
/**
 * Created by PhpStorm.
 * User: Piotrek
 * Date: 21.10.2017
 * Time: 20:06
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Line controller.
 *
 * @Route("admin/users")
 */
class UsersController extends Controller
{
    /**
     * Lists all users entities.
     *
     * @Route("/", name="users_index")
     * @Method("GET")
     */
    function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('users/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Displays a form to edit an existing users entity.
     *
     * @Route("/{id}/edit", name="users_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $users)
    {
        $editForm = $this->createForm('AppBundle\Form\UsersType', $users);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('users_edit', array('id' => $users->getId()));
        }

        return $this->render('users/edit.html.twig', array(
            'users' => $users,
            'edit_form' => $editForm->createView(),
        ));
    }
    /**
     * Finds and displays a users entity.
     *
     * @Route("/{id}", name="users_show")
     * @Method("GET")
     */
    function showAction(User $users)
    {
        $deleteForm = $this->createDeleteForm($users);

        return $this->render('users/show.html.twig', array(
            'users' => $users,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a users entity.
     *
     * @Route("/{id}", name="users_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $users)
    {
        $form = $this->createDeleteForm($users);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($users);
            $em->flush();
        }

        return $this->redirectToRoute('users_index');
    }

    /**
     * Creates a form to delete a users entity.
     *
     * @param User $users The users entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $users)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $users->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}