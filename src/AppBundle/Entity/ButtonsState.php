<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ButtonsState
 *
 * @ORM\Table(name="lines_buttons")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ButtonStateRepository")
 */
class ButtonsState
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Line", inversedBy="buttons", cascade={"all"})
     * @ORM\JoinColumn(name="line_id", referencedColumnName="id")
     */
    private $line;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Button", inversedBy="lines", cascade={"all"})
     * @ORM\JoinColumn(name="button_id", referencedColumnName="id")
     */
    private $button;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;



    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Set line
     *
     * @param \AppBundle\Entity\Line $line
     *
     * @return ButtonsState
     */
    public function setLine(Line $line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get line
     *
     * @return \AppBundle\Entity\Line
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set button
     *
     * @param \AppBundle\Entity\Button $button
     *
     * @return ButtonsState
     */
    public function setButton(Button $button)
    {
        $this->button = $button;

        return $this;
    }

    /**
     * Get button
     *
     * @return \AppBundle\Entity\Button
     */
    public function getButton()
    {
        return $this->button;
    }
}
