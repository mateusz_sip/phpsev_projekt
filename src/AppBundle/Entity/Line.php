<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Line
 *
 * @ORM\Table(name="lines")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LineRepository")
 */
class Line
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="line_name", type="string", length=255)
     */
    private $lineName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ButtonsState", mappedBy="line", cascade={"all"}, fetch="EAGER")
     */
    private $buttons;

    public function __construct()
    {
        $this->buttons = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lineName
     *
     * @param string $lineName
     *
     * @return Line
     */
    public function setLineName($lineName)
    {
        $this->lineName = $lineName;

        return $this;
    }

    /**
     * Get lineName
     *
     * @return string
     */
    public function getLineName()
    {
        return $this->lineName;
    }

    /**
     * Set buttons
     *
     * @param array $buttons
     *
     * @return Line
     */
    public function setButtons($buttons)
    {
        foreach ($buttons as $button) {
            $b = new ButtonsState();

            $b->setLine($this);
            $b->setButton($button);

            $this->addButton($b);
        }
    }

    /**
     * Get buttons
     *
     * @return int
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    public function addButton(ButtonsState $button)
    {
        if ($this->buttons->contains($button)) {
            return;
        }

        $this->buttons->add($button);
    }

    public function removeButton(ButtonsState $button)
    {
        if ($this->buttons->contains($button)) {
            $this->buttons->removeElement($button);
        }

        return $this;
    }

}

