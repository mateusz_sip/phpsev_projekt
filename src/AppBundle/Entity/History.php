<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * History
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HistoryRepository")
 */
class History
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="line_name", type="string", length=255)
     */
    private $lineName;

    /**
     * @var string
     *
     * @ORM\Column(name="button_name", type="string", length=255)
     */
    private $buttonName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duration", type="time", nullable=true)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1, nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lineName
     *
     * @param string $lineName
     *
     * @return History
     */
    public function setLineName($lineName)
    {
        $this->lineName = $lineName;

        return $this;
    }

    /**
     * Get lineName
     *
     * @return string
     */
    public function getLineName()
    {
        return $this->lineName;
    }

    /**
     * Set buttonName
     *
     * @param string $buttonName
     *
     * @return History
     */
    public function setButtonName($buttonName)
    {
        $this->buttonName = $buttonName;

        return $this;
    }

    /**
     * Get buttonName
     *
     * @return string
     */
    public function getButtonName()
    {
        return $this->buttonName;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return History
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return History
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set duration
     *
     * @param \DateTime $duration
     *
     * @return History
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return \DateTime
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param int $status
     *
     * @return History
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

}

