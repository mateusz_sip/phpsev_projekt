<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Button
 *
 * @ORM\Table(name="buttons")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ButtonRepository")
 */
class Button
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="button_name", type="string", length=255)
     */
    private $buttonName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ButtonsState", mappedBy="button", cascade={"all"})
     */
    private $lines;

    public function __construct()
    {
        $this->lines = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get buttonName
     *
     * @return string
     */
    public function getButtonName()
    {
        return $this->buttonName;
    }

    /**
     * Set buttonName
     *
     * @param string $buttonName
     *
     * @return Button
     */
    public function setButtonName($buttonName)
    {
        $this->buttonName = $buttonName;
    }

}

